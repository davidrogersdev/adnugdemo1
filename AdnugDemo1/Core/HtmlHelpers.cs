﻿using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AdnugDemo1.Core
{
    public static class HtmlHelpers
    {
        private const string TypeAttribute = "Attribute";
        private const string JavascriptAttribute = "text/javascript";
        private const string ScriptElement = "script";

        public static IHtmlString GlyphLinkButton(this HtmlHelper helper, string id, string hrefText, string linkText, string glyphClass, object htmlAttributes = null)
        {
            //  First create the anchor element, which will be the parent of the img
            var a = new TagBuilder("a");

            if (!string.IsNullOrWhiteSpace(id))
            {
                a.GenerateId(id);
            }

            a.MergeAttribute("href", hrefText);

            if (!ReferenceEquals(null, htmlAttributes))
            {
                a.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            }

            var i = new TagBuilder("i");

            i.AddCssClass("glyphicon");
            i.AddCssClass(glyphClass);

            //  Get image and set it as child of anchor.
            a.InnerHtml += string.Concat(i.ToString(TagRenderMode.Normal), "&nbsp", linkText);

            // Render/return tag
            return new MvcHtmlString(a.ToString(TagRenderMode.Normal));
        }

        public static IHtmlString Script<T>(this HtmlHelper<T> helper, string inlineScriptContent)
        {
            var builder = new TagBuilder(ScriptElement);

            // Add attributes
            builder.MergeAttribute(TypeAttribute, JavascriptAttribute);

            // note – expression trees not always this simple. In this app, it will be.

            builder.InnerHtml = inlineScriptContent;

            // Render tag.
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));

        }


        public static IHtmlString ScriptRenderer<T, TResult>(this HtmlHelper<T> helper, Expression<Func<T, TResult>> expression)
        {
            var builder = new TagBuilder(ScriptElement);

            // Add attributes
            builder.MergeAttribute(TypeAttribute, JavascriptAttribute);

            // note – expression trees not always this simple. In this app, it will be.
            var body = expression.Body as MemberExpression;

            if (!ReferenceEquals(null, body))
            {
                var propertyName = body.Member.Name;

                var value = string.Empty;
                var model = helper.ViewData.Model;

                if (!ReferenceEquals(null, model))
                {
                    var modelType = typeof(T);
                    var propertyInfo = modelType.GetProperty(propertyName);
                    var propertyValue = propertyInfo.GetValue(model, null);

                    if (!ReferenceEquals(null, propertyValue))
                    {
                        value = propertyValue.ToString();
                    }
                }

                builder.InnerHtml = value;

                // Render tag.
                return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
            }

            return MvcHtmlString.Empty;
        }
    }
}