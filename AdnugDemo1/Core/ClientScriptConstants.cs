﻿
using Yahoo.Yui.Compressor;

namespace AdnugDemo1.Core
{
    public sealed class ClientScriptConstants
    {
        public const string Email = "email";
        public static readonly string EmailConstant = string.Concat("ADNUGDEMO.constants.Email='", Email, "'");
        public const string EmailConfirmationFailed = "email-confirmation-failed";
        public const string LoggedIn = "logged-in";
        public const string LoginFailed = "login-failed";
        public const string MembershipRebootValidationFailed = "membershipReboot-validation-failed";
        public const string PasswordResetConfirmSuccess = "password-reset";
        public const string Result = "result";
        public static readonly string ResultConstant = string.Concat("ADNUGDEMO.constants.Result='", Result, "'");
        public const string ReturnUrl = "returnUrl";
        public static readonly string ReturnUrlConstant = string.Concat("ADNUGDEMO.constants.ReturnUrl='", ReturnUrl, "'");
        public const string ServerSideException = "server-side-exception ";
        public const string Success = "success";
        public static readonly string SuccessConstant = string.Concat("ADNUGDEMO.constants.Success='", Success, "'");
        public const string UserCreated = "user-created";


        public static string GetOrphanedScriptContent()
        {
          var compressor = new JavaScriptCompressor();

          var script = string.Format(@"ADNUGDEMO.constants.result = '{0}';ADNUGDEMO.constants.success = '{1}'", 
              Result, Success
              );

          return compressor.Compress(script);

        } 
    }
}