﻿using System;
using System.Collections.Generic;

namespace AdnugDemo1.Core.JqGrid
{
    public class GridDataPayload
    {
        public string Page { get; set; }
        public string Records { get; set; }
        public int Total { get; set; }
        public UserData UserData { get; set; }

        public IEnumerable<Object> Rows { get; set; }

    }
}