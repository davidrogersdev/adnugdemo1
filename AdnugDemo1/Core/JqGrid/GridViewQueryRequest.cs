﻿
namespace AdnugDemo1.Core.JqGrid
{
    public class GridViewQueryRequest
    {
        public bool PerformSearch { get; set; }

        public long Nd { get; set; } // "newdate" number of milliseconds since January 1, 1970
        public int Rows { get; set; }
        public int Page { get; set; }
        public int Sidx { get; set; } // Field sorted on
        public string Sord { get; set; } // Sort order
    }
}