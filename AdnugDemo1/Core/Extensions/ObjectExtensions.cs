﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AdnugDemo1.Core.Extensions
{
	public static class ObjectExtensions
	{
		public static string ToJsonNet(this object obj)
		{
			var settings = new JsonSerializerSettings()
			{
				// Json.NET will ignore objects in reference loops and not serialize them. 
				// The first time an object is encountered it will be serialized as usual 
				// but if the object is encountered as a child object of itself, the serializer 
				// will skip serializing it.
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
				// maximum depth allowed when reading JSON.
				MaxDepth = 1,
				PreserveReferencesHandling = PreserveReferencesHandling.None
			};

			var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = "dd-MM-yyyy HH:mm:ss" };
			settings.Converters.Add(dateTimeConverter);

			return JsonConvert.SerializeObject(obj, Formatting.Indented, settings);
		}

		public static string ToJsonNet(this object obj, JsonSerializerSettings settings, Formatting formatting, IsoDateTimeConverter dateTimeConverter)
		{
			settings.Converters.Add(dateTimeConverter);
			return JsonConvert.SerializeObject(obj, formatting, settings);
		}
	}
}