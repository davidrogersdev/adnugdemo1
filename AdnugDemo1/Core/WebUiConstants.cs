﻿
namespace AdnugDemo1.Core
{
    public sealed class WebUiConstants
    {
        public const string PasswordManagerTenant = "PasswordManager";
        public const string RememberMe = "Remember Me";
        public const string Success = "success";
    }
}