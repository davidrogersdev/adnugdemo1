﻿using AdnugDemo1.Core;
using AdnugDemo1.Core.Extensions;
using AdnugDemo1.Models;
using BrockAllen.MembershipReboot;
using KesselRun.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace AdnugDemo1.Controllers
{
    public class MembershipController : Controller
    {
        private const string GeneralExceptionMessageForView = "An error has been logged. Please contact bla bla bla ...";
        private readonly AuthenticationService _authenticationService;
        private readonly UserAccountService _userAccountService;

        public MembershipController(AuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
            _userAccountService = _authenticationService.UserAccountService;
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            var loginInputModel = new LoginInputModel
            {
                ClientScript = LoginInputModel.BuildClientScript(),
                Title = "Demo Test This Login Page"
            };

            return View(loginInputModel);
        }


        [HttpPost, AllowAnonymous, ValidateJsonAntiForgeryToken]
        public ActionResult Login(LoginInputModel loginInputModel)
        {
            if (ModelState.IsValid)
            {
                var returnUrl = Server.UrlDecode(loginInputModel.ReturnUrl);

                UserAccount account;
                if (_userAccountService.AuthenticateWithUsernameOrEmail(
                    WebUiConstants.PasswordManagerTenant,
                    loginInputModel.Username,
                    loginInputModel.Password,
                    out account))
                {
                    _authenticationService.SignIn(account, loginInputModel.RememberMe); // loginInputModel.RememberMe

                    return
                        Json(new Dictionary<string, string>
                        {
                            {ClientScriptConstants.Result, ClientScriptConstants.LoggedIn},
                            {ClientScriptConstants.ReturnUrl, returnUrl}
                        });
                }

                ModelState.AddModelError(ClientScriptConstants.LoginFailed, "Invalid Username or Password");
            }

            return this.ModelStateJson(ModelState);
        }

        //[Authorize]
        public ActionResult CreateUser()
        {
            return View(new CreateUserInputModel
            {
                ClientScript = CreateUserInputModel.BuildClientScript(),
                ConfirmPassword = string.Empty,
                Email = string.Empty,
                Password = string.Empty,
                Title = "Register New User",
                Username = string.Empty
            });
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        public ActionResult CreateUser(CreateUserInputModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _userAccountService.CreateAccount(WebUiConstants.PasswordManagerTenant, model.Username,
                        model.Password, model.Email);

                    if (SecuritySettings.Instance.RequireAccountVerification)
                    {
                        return
                            Json(new Dictionary<string, string>
                            {
                                {ClientScriptConstants.Result, ClientScriptConstants.Success},
                                {ClientScriptConstants.Email, model.Email}
                            });
                    }
                    return View(ClientScriptConstants.UserCreated, true);
                }
                catch (ValidationException ex)
                {
                    ModelState.AddModelError(ClientScriptConstants.MembershipRebootValidationFailed, ex.Message);
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(ClientScriptConstants.ServerSideException, exception.Message);
                }

            }
            return this.ModelStateJson(ModelState);
        }

        [AllowAnonymous]
        public ActionResult ConfirmedEmail(string id)
        {
            var account = _userAccountService.GetByVerificationKey(id);

            if (account.HasPassword())
            {
                var confirmedEmailInputModel = new ConfirmedEmailInputModel
                {
                    ClientScript = ConfirmedEmailInputModel.BuildClientScript(),
                    Key = id,
                    Password = string.Empty,
                    Title = "Confirm Email"
                };
                return View(confirmedEmailInputModel);
            }

            return View();
        }

        [HttpPost, AllowAnonymous, ValidateJsonAntiForgeryToken]
        public ActionResult ConfirmedEmail(ConfirmedEmailInputModel confirmedEmailInputModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    UserAccount account;
                    _userAccountService.VerifyEmailFromKey(confirmedEmailInputModel.Key,
                        confirmedEmailInputModel.Password, out account);

                    // We can issue the cookie (sign them in) now that they have verified.
                    _authenticationService.SignIn(account);

                    return
                        Json(new Dictionary<string, string>
                        {
                            {ClientScriptConstants.Result, ClientScriptConstants.LoggedIn},
                            {ClientScriptConstants.ReturnUrl, Path.AltDirectorySeparatorChar.ToString()}
                        });
                }
                catch (ValidationException validationException)
                {
                    ModelState.AddModelError(ClientScriptConstants.EmailConfirmationFailed, validationException.Message);
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(ClientScriptConstants.ServerSideException, exception.Message);
                }
            }

            return this.ModelStateJson(ModelState);
        }

        public ActionResult Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                _authenticationService.SignOut();
                return Json(new Dictionary<string, string>
                {
                    {ClientScriptConstants.Result, ClientScriptConstants.Success},
                    {ClientScriptConstants.ReturnUrl, Path.AltDirectorySeparatorChar.ToString()}
                }, JsonRequestBehavior.AllowGet);
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult PasswordReset()
        {
            var passwordResetConfirmInputModel = new PasswordResetConfirmInputModel
            {
                ClientScript = PasswordResetConfirmInputModel.BuildClientScript(),
                Email = string.Empty,
                Title = "Password Reset Request"
            };

            return View(passwordResetConfirmInputModel);
        }

        [HttpPost, AllowAnonymous, ValidateJsonAntiForgeryToken]
        public ActionResult PasswordReset(PasswordResetConfirmInputModel passwordResetConfirmInputModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var account = _userAccountService.GetByEmail(WebUiConstants.PasswordManagerTenant,
                        passwordResetConfirmInputModel.Email);

                    if (account != null)
                    {
                        _userAccountService.ResetPassword(WebUiConstants.PasswordManagerTenant,
                            passwordResetConfirmInputModel.Email);
                        return Json(new Dictionary<string, string>
                        {
                            {ClientScriptConstants.Result, ClientScriptConstants.PasswordResetConfirmSuccess},
                            {ClientScriptConstants.Email, passwordResetConfirmInputModel.Email}
                        });
                    }

                    ModelState.AddModelError(string.Empty, "Invalid email");
                }
                catch (ValidationException ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            return this.ModelStateJson(ModelState);
        }

        [AllowAnonymous]
        public ActionResult PasswordResetConfirm(string id)
        {
            var passwordResetConfirmInputModel = new ChangePasswordFromResetKeyInputModel
            {
                ClientScript = PasswordResetConfirmInputModel.BuildClientScript(),
                ConfirmPassword = string.Empty,
                Key = id,
                Password = string.Empty,
                Title = "Password Reset Input Screen"
            };

            return View(passwordResetConfirmInputModel);
        }

        [AllowAnonymous]
        [HttpPost, ValidateJsonAntiForgeryToken]
        public ActionResult PasswordResetConfirm(ChangePasswordFromResetKeyInputModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    UserAccount account;
                    if (_userAccountService.ChangePasswordFromResetKey(model.Key, model.Password, out account))
                    {
                        if (account.IsLoginAllowed && !account.IsAccountClosed)
                        {
                            _authenticationService.SignIn(account);

                            return Json(new Dictionary<string, string> { { ClientScriptConstants.Result, ClientScriptConstants.PasswordResetConfirmSuccess } });
                        }
                    }

                }
                catch (ValidationException validationException)
                {
                    ModelState.AddModelError(string.Empty, validationException.Message);
                    
                    //  then log exception here ...
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(string.Empty, GeneralExceptionMessageForView);
                    //  then log exception here ...
                }
            }
            return this.ModelStateJson(ModelState);
        }


        public ActionResult UserClaims()
        {
            var model = new UserClaimsViewModel
            {
                Claims = ((ClaimsIdentity) User.Identity).Claims,
                Title = "User Claims of Logged-in User"
            };

            return View(model);
        }

        [Authorize]
        public ActionResult AddCLaim()
        {
            var claimTypes = typeof (ClaimTypes).GetFields().Where(f => f.IsLiteral).ToList();
            var typesAsSelectList = new List<SelectListItem>(claimTypes.Count);
            string rawConstant;

            foreach (var claimType in claimTypes)
            {
                rawConstant = claimType.GetRawConstantValue().ToString();

                typesAsSelectList.Add(new SelectListItem
                {
                    Text = rawConstant.SubstringFromRight(rawConstant.Length - rawConstant.LastIndexOf(Path.AltDirectorySeparatorChar) - 1),
                    Value = claimType.GetRawConstantValue().ToString()
                });
            }

            var model = new AddClaimInputModel
            {
                ClaimTypes = typesAsSelectList,
                ClientScript = AddClaimInputModel.BuildClientScript(),
                Title = "Add New Claim"
            };

            

            return View(model);
        }

        [HttpPost]
        [ValidateJsonAntiForgeryToken]
        [Authorize]
        public ActionResult AddCLaim(AddClaimInputModel addClaimInputModel)
        {
            if (ModelState.IsValid)
            {
                var claimsIdentityOfLoggedInUser = User.Identity as ClaimsIdentity;

                _userAccountService.AddClaim(
                    claimsIdentityOfLoggedInUser.GetUserID(),
                    addClaimInputModel.NewClaimType,
                    addClaimInputModel.NewClaimValue
                    );
                return Json(new Dictionary<string, string>
                {
                    {ClientScriptConstants.Result, ClientScriptConstants.Success}
                });
            }

            return this.ModelStateJson(ModelState);
        }
    }
}