﻿using System.Diagnostics;
using AdnugDemo1.Core;
using AdnugDemo1.Core.JqGrid;
using AdnugDemo1.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using CredentialsManager.Model;
using CredentialsManager.Services;

namespace AdnugDemo1.Controllers.Api
{
    public class GetAllPasswordsController : ApiController
    {
        private readonly ICredentialsService _credentialsService;

        public GetAllPasswordsController(ICredentialsService credentialsService)
        {
            _credentialsService = credentialsService;
        }

        public HttpResponseMessage Get([FromUri]GridViewQueryRequest model)
        {
            var credentials = _credentialsService.GetCredentials();
            Stopwatch s = new Stopwatch();
            s.Start();
            var passwordDetailsList = GetRows(credentials); 
            s.Stop();
            Trace.WriteLine(string.Format("{0} ticks", (s.ElapsedTicks)));

            var gridDataPayload = new GridDataPayload
            {
                Page = "1",
                Records = "5",
                Total = 1,
                UserData = new UserData {ImportantMessage = "hi world"},
                Rows = passwordDetailsList
            };

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, gridDataPayload);

            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }

        private IEnumerable<Object> GetRows(IEnumerable<Credential> credentials)
        {
            foreach (var credential in credentials)
            {
                var row = new
                {
                    PasswordDetailsId = credential.Id,
                    DetailsRow = new PasswordDetailsViewModel
                    {
                        AssociatedEmail = credential.AssociatedEmail,
                        Comment = credential.Comment,
                        Password = credential.Password,
                        PasswordDetailsId = credential.Id,
                        Site = credential.WebSite.Name,
                        Url = credential.WebSite.Url,
                        UserName = credential.UserName
                    }
                };
                yield return row;
            }
        }

        // GET api/<controller>/5
        //public HttpResponseMessage Get([FromUri]object model)
        //{
        //    return Request.CreateResponse(HttpStatusCode.OK, "hi"); ;
        //}

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}