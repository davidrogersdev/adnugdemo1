﻿using System.Diagnostics;
using AdnugDemo1.Models;
using System.Web.Mvc;

namespace AdnugDemo1.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            Trace.WriteLine(User.Identity.Name);
            Trace.WriteLine(User.Identity.IsAuthenticated);


            return View();
        }
        
        [HttpPost]
        public ActionResult Index(string id)
        {
            return View();
        }
    }
}