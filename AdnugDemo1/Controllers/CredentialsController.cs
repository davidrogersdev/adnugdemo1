﻿using AdnugDemo1.Models;
using CredentialsManager.Model;
using CredentialsManager.Services;
using System.Web.Mvc;

namespace AdnugDemo1.Controllers
{
    public class CredentialsController : Controller
    {
        private readonly ICredentialsService _credentialsService;
        private const string DataValueField = "Id";
        private const string DataTextField = "Name";

        public CredentialsController(ICredentialsService credentialsService)
        {
            _credentialsService = credentialsService;
        }

        public ActionResult CreateCredential()
        {
            var createCredentialInputModel = new CreateCredentialInputModel
            {
                Title = "Create New Credential",
                WebSites = new SelectList(_credentialsService.GetWebSites(), DataValueField, DataTextField)
            };

            return View(createCredentialInputModel);
        }

        [HttpPost]
        public ActionResult CreateCredential(CreateCredentialInputModel model)
        {
            var newCredential = new Credential
            {
                AssociatedEmail = model.AssociatedEmail,
                Comment = model.Comment,
                UserName = model.UserName,
                Password = model.Password,
                WebSiteId = model.Site
            };

            _credentialsService.AddCredential(newCredential);

            model.WebSites = new SelectList(_credentialsService.GetWebSites(), DataValueField, DataTextField); // should pull this from a cache

            return View(model);
        }

        public ActionResult CreateWebSite()
        {
            var webSiteModel = new WebSiteModel
            {
                Name= string.Empty,
                Title = "Add a WebSite"
            };
            return View(webSiteModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult CreateWebSite(WebSiteModel model)
        {
            _credentialsService.AddWebSite(new WebSite
            {
                Name = model.Name,
                Url = model.Url
            });

            return View(model);
        }
    }
}