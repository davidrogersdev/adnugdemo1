
$(function () {

    $('#authenticationsList').jqGrid({
        url: '/api/GetAllPasswords',
        datatype: "json",
        mtype: 'GET',
        colNames: ['Id', 'Site', 'Url', 'UserName', 'Password', 'AssociatedEmail', 'Comment'],
        colModel: [
            { name: 'passwordDetailsId', index: 'passwordDetailsId', width: 55 },
            { name: 'site', index: 'site', width: 90 },
            { name: 'url', index: 'url', width: 300 },
            { name: 'userName', index: 'userName', width: 150, align: "right" },
            { name: 'password', index: 'password', width: 180, align: "right" },
            { name: 'associatedEmail', index: 'associatedEmail', width: 80, align: "right" },
            { name: 'comment', index: 'comment', width: 150, sortable: false }
        ],
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: true,
            cell: "detailsRow",
            id: "passwordDetailsId",
            userdata: "userdata"
        },
        postData: { additionalParameter: 'this is an example and added to queryString' },

        // ***** querystring config ***** //
        prmNames: { search: 'performSearch' }, // customize the names of the queryString variables
        sortname: 'passwordDetailsId',
        rowNum: 10, // the number of rows requested by the Grid.
        sortorder: 'desc',
        // ***** **** ***** //
        
        shrinkToFit: true,
        caption: 'Authentication Details Data',
        height: 'auto',
        loadtext: "Chewing cud ...",
        gridview: true, // perf tweak. Note, incompatible with following: treeGrid or subGrid features, or the afterInsertRow event
        autoencode: true,
        rownumbers: true,

        // ***** pager ***** //
        viewrecords: true, // relevant to pager. Enables the recordtext feature to work. If false, you won't see the "total number of records" in the footer.
        rowList: [2, 5, 10, 20],
        pager: '#gridPager',
        emptyrecords: 'Nothing to display',
        recordtext: 'View {0} - {1} of {2}',
        pgtext: 'Page {0} of {1}'
        // ***** pager ***** //
    });
    $('#authenticationsList').jqGrid('navGrid', '#gridPager', { edit: false, add: false, del: false });

});

