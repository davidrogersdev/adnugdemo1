﻿createNamespace('ADNUGDEMO.utilities.dom');

(function(ns) {

    ns.lightUpValidationSummary = function(id, data) {

        var valSummary = $('#' + id);
        valSummary.removeClass('validation-summary-valid').addClass('validation-summary-errors');

        var errorsList = valSummary.find('ul');
        errorsList.empty();

        for (var error in data.data) {
            if (Object.prototype.hasOwnProperty.call(data.data, error)) {
                errorsList.append('<li>' + data.data[error] + '</li>');
                console.log(data.data[error]);
            }
        }
    };
}(ADNUGDEMO.utilities.dom));

createNamespace('ADNUGDEMO.utilities.gen');

(function(ns) {
    ns.defaults = function(d) {
        return function(o, k) {
            var val = ns.noNull(_.identity, d[k]);
            return o && val(o[k]);
        };
    };

    ns.noNull = function(fun /*, defaults */) {
        var defaults = _.rest(arguments);
        return function( /* args */) {
            var args = _.map(arguments, function(e, i) {
                return ns.extant(e) ? e : defaults[i];
            });
            return fun.apply(null, args);
        };
    };

    ns.extant = function(x) {
        return x != null;
    };
}(ADNUGDEMO.utilities.gen));