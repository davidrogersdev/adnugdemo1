﻿/// <reference path="../../purl.js" />

createNamespace('ADNUGDEMO.utilities.pathProcessor');

(function (ns) {

    ns.getBaseUrl = function () {
        var fullUrl = $.url().attr(ADNUGDEMO.constants.source);
        var indexOfRelativePath = fullUrl.indexOf($.url().attr('relative'));
        return fullUrl.substr(0, indexOfRelativePath);
    };

    ns.getCurrentUrl = function () {
        return $.url().attr(ADNUGDEMO.constants.source);
    };

    ns.getReturnUrl = function () {
        return $.url().param('ReturnUrl');
    };

    ns.getUrlFromElement = function (element) {
        return $(element).url().attr(ADNUGDEMO.constants.source);
    };

    ns.getUrlFromForm = function (formAsJqueryObject) {
        return formAsJqueryObject.attr('action');
    };

    ns.goToUrl = function (url) {
        window.location.href = ns.getBaseUrl() + url;
    };

    ns.goToHomeUrl = function () {
        window.location.href = ns.getBaseUrl();
    };

    ns.goToReturnUrl = function (urlObj) {

        //  This ensures that the page will always have a target if returnUrl is null or undefined.
        //  The base url will be the default location.
        var defaultObj = {};
        defaultObj[ADNUGDEMO.constants.ReturnUrl] = '/';

        var lookup = ADNUGDEMO.utilities.gen.defaults(defaultObj);

        window.location.href = lookup(urlObj, ADNUGDEMO.constants.ReturnUrl);
    };

})(ADNUGDEMO.utilities.pathProcessor);

