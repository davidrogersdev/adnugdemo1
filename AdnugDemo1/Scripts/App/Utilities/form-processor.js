﻿createNamespace('ADNUGDEMO.utilities.formProcessor');

(function(ns) {

    ns.processInputs = function(inputs) {

        var dataAsArrays = _.map(inputs, function(input) {
            var wrapInput = $(input);
            return [wrapInput.attr('id'), wrapInput.val()];
        });

        return _.object(dataAsArrays);

    };

    ns.clearInputs = function(inputs) {
        _.each(inputs, function(input) {
            var wrapInput = $(input);
            wrapInput.val('');
        });
    };

    ns.getApplicableInputs = function (formId) {

        var form = $('#' + formId);
        var checkBoxResult = ns.getCheckBoxInput(form);
        var inputs = form.find('input, textarea, select').not(':input[type=button], :input[type=submit], :input[type=reset], :input[name="__RequestVerificationToken"], :input[type=checkbox]');

        //inputs.add(checkBoxResult);

        return inputs;
    };

    ns.getCheckBoxInput = function(formJqueryObject) {
        var checkbox = formJqueryObject.find('input[type=checkbox]');

        var hiddenField = checkbox.next();

        return hiddenField;
    };

})(ADNUGDEMO.utilities.formProcessor);



