﻿var ADNUGDEMO = {
    constants: {
        click: 'click',
        errorStatus: 'error',
        jsonContentType: 'application/json',
        jsonDataType: 'json',
        loadingId: '#Loading',
        loginSubmitButton: '#loginSubmitButton',
        postRequestType: 'POST',
        requestVerificationToken: '__RequestVerificationToken',
        source: 'source',
        success: 'success'
    },
    snippets: {
        ajaxSendingLabelStart: '<span id="feedbackLabel" class="label label-warning">&nbsp;<i class="icon-spinner icon-spin "></i>&nbsp;',
        ajaxSendingLabelError: '<span id="feedbackLabel" class="label label-danger">&nbsp;<i class="fa fa-exclamation-circle"></i>&nbsp;',
        ajaxSendingLabelSuccess: '<span id="feedbackLabel" class="label label-success">&nbsp',
        ajaxSendingLabelClose: '</span>'
    }
};

ADNUGDEMO.constants.verificationTokenSelector = '[name=' + ADNUGDEMO.constants.requestVerificationToken + ']';