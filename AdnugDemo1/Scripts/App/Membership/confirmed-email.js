/// <reference path="../Utilities/formProcessor.js" />
/// <reference path="../Constants.js" />

$(function () {
    var loginMsgLabelWrap = $('#loginMsgLabelWrap');
    loginMsgLabelWrap.hide();
    $('#Password').focus();

    $('#ConfirmEmailButton').on(ADNUGDEMO.constants.click, function (e) {

        e.preventDefault();

        var url = ADNUGDEMO.utilities.pathProcessor.getUrlFromForm($('#ConfirmUser > form'));

        //  First, sort out the Antiforgery token for json POST
        var token = $(ADNUGDEMO.constants.verificationTokenSelector).val();
        var headers = {};
        headers[ADNUGDEMO.constants.requestVerificationToken] = token;

        var inputs = ADNUGDEMO.utilities.formProcessor.getApplicableInputs('confirmedEmailForm');

        var payload = ADNUGDEMO.utilities.formProcessor.processInputs(inputs);

        $.ajax({
            url: url,
            type: ADNUGDEMO.constants.postRequestType,
            dataType: ADNUGDEMO.constants.jsonDataType,
            contentType: ADNUGDEMO.constants.jsonContentType,
            data: JSON.stringify(payload),
            headers: headers,
            beforeSend: function (xhr) {
                loginMsgLabelWrap.show();
                loginMsgLabelWrap.html('<span id="signingInMsg" class="label label-default">&nbsp;<i class="icon-spinner icon-spin "></i>&nbsp;Signing in ...</span>');
            }
        }).done(function (data, textStatus, jqXHR) {
            if (data[ADNUGDEMO.constants.Result] === ADNUGDEMO.constants.LoggedIn) {
                var urlObj = {};
                urlObj[ADNUGDEMO.constants.ReturnUrl] = data[ADNUGDEMO.constants.ReturnUrl];
                ADNUGDEMO.utilities.pathProcessor.goToReturnUrl(urlObj);
            } else if (!data.isSuccessful) {
                loginMsgLabelWrap.html('<span id="signingInMsg" class="label label-danger">&nbsp;<i class="fa fa-exclamation-circle"></i>&nbsp;Error ...</span>');
                ADNUGDEMO.utilities.dom.lightUpValidationSummary('confirmedEmailValidationSummary', data);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            loginMsgLabelWrap.html('<span id="signingInMsg" class="label label-danger">&nbsp;<i class="fa fa-exclamation-circle"></i>&nbsp;Server Error ...</span>');
        }).always(function (data) {

        });
    });
});

/**/