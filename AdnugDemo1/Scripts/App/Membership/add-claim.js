/// <reference path="../Utilities/formProcessor.js" />
/// <reference path="../Constants.js" />

$(function () {

    var stockClaimsVisible = true;
    var addClaimMsgLabelWrap = $('#addClaimMsgLabelWrap');

    $('#addClaimLabel').on(ADNUGDEMO.constants.click, function (e) {

        e.preventDefault();

        if (stockClaimsVisible) {
            $('#stockClaimsWrapper').fadeOut(500, function() {
                $('#customClaimWrapper').show();
            });

            stockClaimsVisible = false;
            $(this).text('Pick Stock Claim');
        } else {
            $('#customClaimWrapper').fadeOut(500, function () {
                $('#stockClaimsWrapper').show();
            });
            stockClaimsVisible = true;
            $(this).text('Add custom claim');
        }

        
    });

    $('#createClaimButton').on(ADNUGDEMO.constants.click, function (e) {
        e.preventDefault();

        var newClaimType = $('#NewClaimType');
        var url = ADNUGDEMO.utilities.pathProcessor.getUrlFromForm($('#addClaimForm'));

        //  First, sort out the Antiforgery token for json POST
        var token = $(ADNUGDEMO.constants.verificationTokenSelector).val();
        var headers = {};
        headers[ADNUGDEMO.constants.requestVerificationToken] = token;

        if ($('#stockClaimsWrapper').is(':visible')) {
            newClaimType.val($('#SelectedClaimType').val());
        } else {
            newClaimType.val($('#CustomClaimType').val());
        }

        var inputs = ADNUGDEMO.utilities.formProcessor.getApplicableInputs('addClaimForm');

        var payload = ADNUGDEMO.utilities.formProcessor.processInputs(inputs);

        $.ajax({
            url: url,
            type: ADNUGDEMO.constants.postRequestType,
            data: JSON.stringify(payload),
            dataType: ADNUGDEMO.constants.jsonDataType,
            contentType: ADNUGDEMO.constants.jsonContentType,
            headers: headers,
            beforeSend: function(xhr) {
                addClaimMsgLabelWrap.show();
                addClaimMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelStart + 'Adding claim ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
            }
        }).done(function(data, textStatus, jqXHR) {
            if (data[ADNUGDEMO.constants.Result] === ADNUGDEMO.constants.success) {
                addClaimMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelSuccess + 'Claim added!' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
            } else {
                addClaimMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelError + 'Error ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            addClaimMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelError + 'Server Error ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
        });

    });
});