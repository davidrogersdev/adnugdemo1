/// <reference path="../Utilities/formProcessor.js" />
/// <reference path="../Constants.js" />

$(function() {
    var registerMsgLabelWrap = $('#registerMsgLabelWrap');
    registerMsgLabelWrap.hide();


    $('#Username').focus();

    $('#registerNewUserButton').on(ADNUGDEMO.constants.click, function(e) {

        var url = $('#registerUser > form').attr('action');

        e.preventDefault();

        //  First, sort out the Antiforgery token for json POST
        var token = $(ADNUGDEMO.constants.verificationTokenSelector).val();
        var headers = {};
        headers[ADNUGDEMO.constants.requestVerificationToken] = token;

        var inputs = ADNUGDEMO.utilities.formProcessor.getApplicableInputs('createUserForm');

        var payload = ADNUGDEMO.utilities.formProcessor.processInputs(inputs);

        $.ajax({
            url: url,
            type: ADNUGDEMO.constants.postRequestType,
            dataType: ADNUGDEMO.constants.jsonDataType,
            contentType: ADNUGDEMO.constants.jsonContentType,
            data: JSON.stringify(payload),
            headers: headers,
            beforeSend: function (xhr) {

                registerMsgLabelWrap.show();
                registerMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelStart + 'Creating User ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
                
                var valSummary = $('#registerValidationSummary');
                valSummary.removeClass('validation-summary-errors').addClass('validation-summary-valid');

                var errorsList = valSummary.find('ul');
                errorsList.empty();
                errorsList.append('<li style="display:none"></li>');
            }
        }).done(function(data) {
            if (data[ADNUGDEMO.constants.Result] === ADNUGDEMO.constants.success) {
                $('#registerUser').hide();
                $('#operationResult').fadeIn(500);
                $('#emailText').text(data.email);

                $('#createAnotherUserButton').on('click', function(e) {
                    $('#registerUser').fadeIn(500);
                    ADNUGDEMO.utilities.formProcessor.clearInputs(inputs);
                    $('#operationResult').hide();
                    registerMsgLabelWrap.hide();
                });
                registerMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelStart + 'Redirecting you now ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
            } else if(!data.isSuccessful) {
                registerMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelError + 'Error ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
                ADNUGDEMO.utilities.dom.lightUpValidationSummary('registerValidationSummary', data);
            }
        }).fail(function(data) {
            registerMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelError + 'Server Error ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
        }).always(function(data) {

        });
    });
});

/**/