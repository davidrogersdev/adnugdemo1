/// <reference path="../ADNUGDEMO.utilities/formProcessor.js" />
/// <reference path="../Constants.js" />

$(function () {
    var loginMsgLabelWrap = $('#loginMsgLabelWrap');
    loginMsgLabelWrap.hide();
    $('#Email').focus();

    $('#passwordResetConfirmSubmitButton').on('click', function (e) {

        var url = $('#resetPasswordSection > form').url().attr(ADNUGDEMO.constants.source);

        e.preventDefault();

        //  First, sort out the Antiforgery token for json POST
        var token = $(ADNUGDEMO.constants.verificationTokenSelector).val();
        var headers = {};
        headers[ADNUGDEMO.constants.requestVerificationToken] = token;

        var inputs = ADNUGDEMO.utilities.formProcessor.getApplicableInputs('passwordReset');

        var payload = ADNUGDEMO.utilities.formProcessor.processInputs(inputs);

        $.ajax({
            url: url,
            type: ADNUGDEMO.constants.postRequestType,
            data: JSON.stringify(payload),
            dataType: ADNUGDEMO.constants.jsonDataType,
            contentType: ADNUGDEMO.constants.jsonContentType,
            headers: headers,
            beforeSend: function (xhr) {
                loginMsgLabelWrap.show();
                loginMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelStart + 'Sending now ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
            }
        }).done(function (data, textStatus, jqXHR) {
            if (data[ADNUGDEMO.constants.Result] === ADNUGDEMO.constants.PasswordWasReset) {
                $('#resetPasswordSection').hide();
                $('#operationResult').fadeIn(500);
                $('#emailText').text(data.email);
            } else if (!data.isSuccessful) {
                loginMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelError + 'Error ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
                ADNUGDEMO.utilities.dom.lightUpValidationSummary('passwordResetValidationSummary', data);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            loginMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelError + 'Server Error ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
        }).always(function (data) {

        });
    });

    $('#passwordResetSubmitButton').on('click', function (e) {

        var url = $('#passwordResetForm').url().attr(ADNUGDEMO.constants.source);

        e.preventDefault();

        //  First, sort out the Antiforgery token for json POST
        var token = $(ADNUGDEMO.constants.verificationTokenSelector).val();
        var headers = {};
        headers[ADNUGDEMO.constants.requestVerificationToken] = token;

        var inputs = ADNUGDEMO.utilities.formProcessor.getApplicableInputs('passwordResetForm');

        var payload = ADNUGDEMO.utilities.formProcessor.processInputs(inputs);

        $.ajax({
            url: url,
            type: ADNUGDEMO.constants.postRequestType,
            data: JSON.stringify(payload),
            dataType: ADNUGDEMO.constants.jsonDataType,
            contentType: ADNUGDEMO.constants.jsonContentType,
            headers: headers,
            beforeSend: function (xhr) {
                loginMsgLabelWrap.show();
                loginMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelStart + 'Sending now ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
            }
        }).done(function (data, textStatus, jqXHR) {
            if (data[ADNUGDEMO.constants.Result] === ADNUGDEMO.constants.PasswordWasReset) {
                ADNUGDEMO.utilities.pathProcessor.goToHomeUrl();
            } else if (!data.isSuccessful) {
                loginMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelError + 'Error ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
                ADNUGDEMO.utilities.dom.lightUpValidationSummary('passwordResetValidationSummary', data);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            loginMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelError + 'Server Error ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
        }).always(function (data) {

        });
    });

});

/**/