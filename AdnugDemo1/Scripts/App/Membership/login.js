/// <reference path="../Utilities/formProcessor.js" />
/// <reference path="../Constants.js" />
/// <reference path="../Utilities/pathProcessor.js" />

$(function() {
    var loginMsgLabelWrap = $('#loginMsgLabelWrap');
    loginMsgLabelWrap.hide();

    $('#Username').focus();

    $(ADNUGDEMO.constants.loginSubmitButton).on(ADNUGDEMO.constants.click, function (e) {

        e.preventDefault();

        var url = ADNUGDEMO.utilities.pathProcessor.getUrlFromForm($('#loginUser > form'));

        //  First, sort out the Antiforgery token for json POST
        var token = $(ADNUGDEMO.constants.verificationTokenSelector).val();
        var headers = {};
        headers[ADNUGDEMO.constants.requestVerificationToken] = token;

        var inputs = ADNUGDEMO.utilities.formProcessor.getApplicableInputs('logInForm');

        var payload = ADNUGDEMO.utilities.formProcessor.processInputs(inputs);
        
        payload.returnUrl = ADNUGDEMO.utilities.pathProcessor.getReturnUrl();

        $.ajax({
            url: url,
            type: ADNUGDEMO.constants.postRequestType,
            data: JSON.stringify(payload),
            dataType: ADNUGDEMO.constants.jsonDataType,
            contentType: ADNUGDEMO.constants.jsonContentType,
            headers: headers,
            beforeSend: function(xhr) {
                loginMsgLabelWrap.show();
                loginMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelStart + 'Signing in ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
            }
        }).done(function (data, textStatus, jqXHR) {
            if (data[ADNUGDEMO.constants.Result] === ADNUGDEMO.constants.LoggedIn) {
                loginMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelStart + 'Redirecting you now ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
                var urlObj = {};
                urlObj[ADNUGDEMO.constants.ReturnUrl] = data[ADNUGDEMO.constants.ReturnUrl];
                ADNUGDEMO.utilities.pathProcessor.goToReturnUrl(urlObj);
            } else if (!data.isSuccessful) {
                loginMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelError + 'Error ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
                ADNUGDEMO.utilities.dom.lightUpValidationSummary('loginValidationSummary', data);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            loginMsgLabelWrap.html(ADNUGDEMO.snippets.ajaxSendingLabelError + 'Server Error ...' + ADNUGDEMO.snippets.ajaxSendingLabelClose);
        }).always(function (data, textStatus, jqXHR) {
            
        });
    });

    $('#rememberMeCheckbox').on('click', function (e) {
        $(this).next().val(this.checked);
    });

    $('#rememberMeCheckbox').next().attr('id', 'RememberMe');
});
