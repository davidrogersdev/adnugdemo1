/// <reference path="../Utilities/formProcessor.js" />
/// <reference path="../Constants.js" />
/// <reference path="../Utilities/pathProcessor.js" />

$(function() {
    
    $('#loginoutlink').on(ADNUGDEMO.constants.click, function (e) {

        e.preventDefault();

        var fun = $.trim($(this).text());

        if (fun === 'Log Out') {

            var url = ADNUGDEMO.utilities.pathProcessor.getUrlFromElement(this); // 'this' is an anchor element

            $.ajax({
                url: url,
                type: ADNUGDEMO.constants.postRequestType,
                data: '',
                dataType: ADNUGDEMO.constants.jsonDataType,
                contentType: ADNUGDEMO.constants.jsonContentType,
                beforeSend: function(xhr) {
                    
                }
            }).done(function (data, textStatus, jqXHR) {
                if (data[ADNUGDEMO.constants.result] === ADNUGDEMO.constants.success) {
                    
                    var urlObj = {};
                    urlObj[ADNUGDEMO.constants.ReturnUrl] = data.returnUrl;
                    ADNUGDEMO.utilities.pathProcessor.goToReturnUrl(urlObj);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {

            }).always(function(data) {
                
            });
        } else {
            ADNUGDEMO.utilities.pathProcessor.goToUrl('/Membership/Login');
        }
    });
});
