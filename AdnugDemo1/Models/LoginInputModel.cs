﻿using System.ComponentModel.DataAnnotations;
using AdnugDemo1.Core;
using Yahoo.Yui.Compressor;

namespace AdnugDemo1.Models
{
    public class LoginInputModel : AdnugDemoModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [Required]
        public bool RememberMe { get; set; }

        [ScaffoldColumn(false)]
        public string ReturnUrl { get; set; }

        public static string BuildClientScript()
        {
            var compressor = new JavaScriptCompressor();
            var script = string.Format(
            @"ADNUGDEMO.constants.LoggedIn = '{0}';
              {1};{2};", ClientScriptConstants.LoggedIn, ClientScriptConstants.ReturnUrlConstant, ClientScriptConstants.ResultConstant);

            return compressor.Compress(script);
        }
    }
}