﻿using System.ComponentModel.DataAnnotations;
using AdnugDemo1.Core;
using Yahoo.Yui.Compressor;

namespace AdnugDemo1.Models
{
    public class CreateUserInputModel : AdnugDemoModel
    {
        [ScaffoldColumn(false)]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Password confirmation must match password.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public static string BuildClientScript()
        {
            var compressor = new JavaScriptCompressor();
            var script = string.Format(
            @"  {0};{1};{2};", ClientScriptConstants.ResultConstant, ClientScriptConstants.SuccessConstant, ClientScriptConstants.EmailConstant);

            return compressor.Compress(script);
        }

    }
}