﻿using System.ComponentModel.DataAnnotations;

namespace AdnugDemo1.Models
{
    public abstract class AdnugDemoModel
    {
        [ScaffoldColumn(false)]
        public string Title { get; set; }

        [ScaffoldColumn(false)]
        public string ClientScript { get; set; }

    }
}