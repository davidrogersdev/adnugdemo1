﻿
using System.ComponentModel.DataAnnotations;

namespace AdnugDemo1.Models
{
    public class PasswordDetailsViewModel
    {
        public int PasswordDetailsId { get; set; }

        public string AssociatedEmail { get; set; }
        public string Comment { get; set; }
        public string Password { get; set; }
        public string Site { get; set; }
        public string Url { get; set; }
        public string UserName { get; set; }
    }
}