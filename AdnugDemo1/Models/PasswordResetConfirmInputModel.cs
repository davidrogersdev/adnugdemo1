﻿using System.ComponentModel.DataAnnotations;
using AdnugDemo1.Core;
using Yahoo.Yui.Compressor;

namespace AdnugDemo1.Models
{
    public class PasswordResetConfirmInputModel : AdnugDemoModel
    {
        [Required]
        [EmailAddress(ErrorMessage="Please enter a valid email address")]
        public string Email { get; set; }

        public static string BuildClientScript()
        {
            var compressor = new JavaScriptCompressor();
            var script = string.Format(@"ADNUGDEMO.constants.PasswordWasReset = '{0}';{1};", ClientScriptConstants.PasswordResetConfirmSuccess, ClientScriptConstants.ResultConstant);

            return compressor.Compress(script);
        }
    }
}