﻿
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AdnugDemo1.Models
{
    public class CreateCredentialInputModel : AdnugDemoModel
    {
        [EmailAddress]
        public string AssociatedEmail { get; set; }
        public string Comment { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public int Site { get; set; }
        [Required]
        public string UserName { get; set; }

        // For the DropDownList
        [DisplayName("Web site")]
        [Required]
        public IEnumerable<SelectListItem> WebSites { get; set; }
    }
}