using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AdnugDemo1.Core;
using Yahoo.Yui.Compressor;

namespace AdnugDemo1.Models
{
    public class AddClaimInputModel : AdnugDemoModel
    {
        [DisplayName(".NET ClaimTypes")]
        public IEnumerable<SelectListItem> ClaimTypes { get; set; }
        [DisplayName("Custom ClaimType")]
        public string CustomClaimType { get; set; }
        [Required]
        [HiddenInput]
        public string NewClaimType { get; set; }
        [Required]
        [DisplayName("Value of New Claim")]
        public string NewClaimValue { get; set; }
        public string SelectedClaimType { get; set; }

        public static string BuildClientScript()
        {
            var compressor = new JavaScriptCompressor();
            var script = string.Format(
            @"ADNUGDEMO.constants.Result = '{0}';", ClientScriptConstants.Result);

            return compressor.Compress(script);
        }
    }
}