﻿
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AdnugDemo1.Core;
using Yahoo.Yui.Compressor;

namespace AdnugDemo1.Models
{
    public class ChangePasswordFromResetKeyInputModel : AdnugDemoModel
    {
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Password confirmation must match password.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [HiddenInput]
        public string Key { get; set; }

        public static string BuildClientScript()
        {
            var compressor = new JavaScriptCompressor();
            var script = string.Format(@"ADNUGDEMO.constants.PasswordWasReset = '{0}';", ClientScriptConstants.PasswordResetConfirmSuccess, ClientScriptConstants.ResultConstant);

            return compressor.Compress(script);
        }

    }
}