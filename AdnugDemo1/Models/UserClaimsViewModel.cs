﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;

namespace AdnugDemo1.Models
{
    public class UserClaimsViewModel : AdnugDemoModel
    {
        [DisplayName(".NET Claim Types")]
        [Required]
        public IEnumerable<Claim> Claims { get; set; }

        public string CustomClaimType { get; set; }
    }
}