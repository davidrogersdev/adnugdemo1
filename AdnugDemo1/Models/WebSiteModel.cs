﻿using System.ComponentModel.DataAnnotations;

namespace AdnugDemo1.Models
{
    public class WebSiteModel : AdnugDemoModel
    {
        [Required]
        public string Name{ get; set; }
        [Required]
        public string Url{ get; set; }

    }
}