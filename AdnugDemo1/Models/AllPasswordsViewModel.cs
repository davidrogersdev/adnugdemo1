﻿using System.Collections.Generic;

namespace AdnugDemo1.Models
{
    public class AllPasswordsViewModel : AdnugDemoModel
    {
        public IEnumerable<PasswordDetailsViewModel> PasswordDetailsViewModels { get; set; }
    }
}