﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AdnugDemo1.Core;
using Yahoo.Yui.Compressor;

namespace AdnugDemo1.Models
{
    public class ConfirmedEmailInputModel : AdnugDemoModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Enter your password to confirm your email address")]
        public string Password { get; set; }

        [HiddenInput]
        public string Key { get; set; }

        public static string BuildClientScript()
        {
            var compressor = new JavaScriptCompressor();
            var script = string.Format(
            @"  {0};
                ADNUGDEMO.constants.LoggedIn = '{1}';
                {2};", ClientScriptConstants.ResultConstant, ClientScriptConstants.LoggedIn, ClientScriptConstants.ReturnUrlConstant);

            return compressor.Compress(script);
        }
    }
}