﻿using System.Web.Optimization;
using AdnugDemo1.Core;

namespace AdnugDemo1
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/i18n/grid.locale-en.js", // necessary for jqGrid
                "~/Scripts/jquery.jqGrid.*" // necessary for jqGrid
                ));

            bundles.Add(new ScriptBundle("~/bundles/other-libs").Include(
                "~/Scripts/App/Utilities/crockford.js",
                "~/Scripts/underscore-{version}.js",
                "~/Scripts/purl.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/clientside-constants").Include(
                "~/Scripts/App/constants.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"
                ));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"
                ));

            bundles.Add(new ScriptBundle("~/bundles/utilities").Include(
                "~/Scripts/App/Utilities/core.js",
                "~/Scripts/App/Utilities/path-processor.js",
                "~/Scripts/App/Utilities/form-processor.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/add-claim").Include(
                "~/Scripts/App/Membership/add-claim.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/login").Include(
                "~/Scripts/App/Membership/login.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/register").Include(
                "~/Scripts/App/Membership/register.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/password-reset").Include(
                "~/Scripts/App/Membership/password-reset.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/stored-credentials").Include(
                "~/Scripts/App/stored-credentials.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/confirmed-email").Include(
                "~/Scripts/App/Membership/confirmed-email.js"
                ));
            
            bundles.Add(new ScriptBundle("~/bundles/logout").Include(
                "~/Scripts/App/Membership/logout.js"
                ));

            /******************************* CSS *******************************/
            bundles.Add(new StyleBundle("~/Content/site-and-membership").Include(
                "~/Content/Site.css",
                "~/Content/membership/Membership.css"
                ));

            bundles.Add(new StyleBundle("~/Content/bootstrap-label").Include(
                "~/Content/bootstrap/bootstrap-label.css"
                ));

            bundles.Add(new StyleBundle("~/Content/awesome-font").Include(
                "~/Content/font-awesome/font-awesome.css", 
                new CssRewriteUrlTransformWrapper()
                ));

            bundles.Add(new StyleBundle("~/Content/jqGrid-styles").Include(
                "~/Content/themes/base/theme.css",
                "~/Content/jquery.jqGrid/ui.jqgrid.css"
                ));

            bundles.Add(new StyleBundle("~/Content/pure-css-by-yahoo").Include(
                "~/Content/pure-css/pure.css"
                ));


            BundleTable.EnableOptimizations = false;

        }
    }
}