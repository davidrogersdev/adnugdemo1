﻿
namespace CredentialsManager.Model
{
    public class WebSite
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
