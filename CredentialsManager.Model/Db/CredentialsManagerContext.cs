﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace CredentialsManager.Model.Db
{
    public class CredentialsManagerContext : DbContext
    {
        public CredentialsManagerContext()
            : base("Name=CredentialsManagerContext")
        {
        }

        public DbSet<Credential> Credentials { get; set; }
        public DbSet<WebSite> WebSites { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Credential>().HasKey(credential => credential.Id);
            modelBuilder.Entity<Credential>()
                .Property(credential => credential.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<WebSite>().HasKey(webSite => webSite.Id);
            modelBuilder.Entity<WebSite>()
                .Property(webSite => webSite.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            base.OnModelCreating(modelBuilder);
        }
    }
}
