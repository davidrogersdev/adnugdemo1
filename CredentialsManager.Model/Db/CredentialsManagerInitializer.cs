﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace CredentialsManager.Model.Db
{
    //public class CredentialsManagerInitializer : DropCreateDatabaseIfModelChanges<CredentialsManagerContext>
    public class CredentialsManagerInitializer : DropCreateDatabaseAlways<CredentialsManagerContext>
    {
        protected override void Seed(CredentialsManagerContext context)
        {
            context.Configuration.LazyLoadingEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;
            context.Configuration.ProxyCreationEnabled = false;

            var sitesList = new List<WebSite>
            {
                new WebSite
                {
                    Name = "GMail",
                    Url = "https://mail.google.com"
                },
                new WebSite
                {
                    Name = "Live Mail",
                    Url = "https://live.com.au"
                },
                new WebSite
                {
                    Name = "Yahoo Mail",
                    Url = "https://au.mail.yahoo.com/"
                },
                new WebSite
                {
                    Name = "Outlook",
                    Url = "https://outlook.com"
                },
                new WebSite
                {
                    Name = "BitBucket",
                    Url = "http://bitbucket.org"
                },
                new WebSite
                {
                    Name = "Bitly",
                    Url = "http://bitly.com"
                }
            };

            sitesList.ForEach(webSite => context.WebSites.Add(webSite));
            context.SaveChanges();

            var credentialsList = new List<Credential>
            {
                new Credential
                {
                    AssociatedEmail = "davidrogersau@yahoo.com.au",
                    Password = "ek10g5wnqm0wa",
                    WebSiteId = 1,
                    UserName = "rogersdave"
                }
                ,
                new Credential
                {
                    AssociatedEmail = "davidalanrogers@live.com.au",
                    Password = "jy3rgq2b5",
                    WebSiteId = 3,
                    UserName = "davidrogersau"
                },
                new Credential
                {
                    AssociatedEmail = "davidrogersau@yahoo.com.au",
                    Password = "rszxew1cetqzyf1",
                    WebSiteId= 6,
                    UserName = "davidrogers"
                },
                new Credential
                {
                    AssociatedEmail = "davidalanrogers@live.com.au",
                    Password = "5h3d2agm31l",
                    WebSiteId= 4,
                    UserName = "rogersdavid"
                },
                new Credential
                {
                    AssociatedEmail = "davidalanrogers@live.com.au",
                    Password = "hvy50wwme3koz",
                    WebSiteId = 5,
                    UserName = "davidrogersman"
                },
                new Credential
                {
                    AssociatedEmail = "rogers.dave@gmail.com",
                    Password = "xqojvfle1cjwr",
                    WebSiteId = 2,
                    UserName = "davidalrogers"
                }
            };

            credentialsList.ForEach(credential => context.Credentials.Add(credential));

            base.Seed(context);
        }
    }
}
