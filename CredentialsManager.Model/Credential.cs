﻿
namespace CredentialsManager.Model
{
    public class Credential
    {
        public int Id { get; set; }

        public string AssociatedEmail { get; set; }
        public string Comment { get; set; }
        public string Password { get; set; }
        public int WebSiteId { get; set; }
        public WebSite WebSite { get; set; }
        public string UserName { get; set; }
    }
}
