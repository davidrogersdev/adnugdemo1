﻿using System.Collections.Generic;
using CredentialsManager.Model;

namespace CredentialsManager.Services
{
    public interface ICredentialsService
    {
        int AddCredential(Credential credential);
        int AddWebSite(WebSite webSite);
        IEnumerable<Credential> GetCredentials();
        IEnumerable<WebSite> GetWebSites();
    }
}