﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CredentialsManager.Model;
using CredentialsManager.Model.Db;

namespace CredentialsManager.Services
{
    public class CredentialsService : ICredentialsService
    {
        private readonly CredentialsManagerContext _context;

        public CredentialsService(CredentialsManagerContext context)
        {
            _context = context;
        }

        public int AddCredential(Credential credential)
        {
            _context.Credentials.Add(credential);
            return _context.SaveChanges();
        }

        public int AddWebSite(WebSite webSite)
        {
            _context.WebSites.Add(webSite);
            return _context.SaveChanges();
        }

        public IEnumerable<Credential> GetCredentials()
        {
            return _context.Set<Credential>().Include(credential => credential.WebSite);
        }

        public IEnumerable<WebSite> GetWebSites()
        {
            return _context.Set<WebSite>().ToList();
        }
    }
}
